package com.msalah.esferiat2.model


data class MonthlyQuestion (

    val qID: String?,
    val qDescreption: String?,
    val qAnswer: String?,
    val qDescreptionAr: String?

)
