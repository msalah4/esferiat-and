package com.msalah.esferiat2

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.msalah.esferiat2.apis.HomeApi
import com.msalah.esferiat2.model.LoggedInUser
import com.msalah.esferiat2.utils.Defaults
import kotlinx.android.synthetic.main.activity_update_info_confirmation_code.*
import kotlinx.coroutines.*
import org.jetbrains.anko.longToast

class UpdateInfoConfirmationCode : AppCompatActivity() {

    lateinit var progress: ProgressDialog
    lateinit var loggedInUser: LoggedInUser
    lateinit var code: String
    lateinit var type: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_info_confirmation_code)

        loggedInUser = Defaults.getLoggedInUser(this)
        val mainActivityJob = Job()
        type = intent.getStringExtra("type")
        progress = ProgressDialog(this)
        progress.setTitle("جاري التحميل")
        progress.setCancelable(false) // disable dismiss by tapping outside of the dialog

        progress.show()
        //2 Handle exceptions if any
        val errorHandler = CoroutineExceptionHandler { _, exception ->
            progress.dismiss()
            AlertDialog.Builder(this).setTitle("Error")
                .setMessage(exception.message)
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }

        //3 the Coroutine runs using the Main (UI) dispatcher
        val coroutineScope = CoroutineScope(mainActivityJob + Dispatchers.Main)
        val context = this
        coroutineScope.launch(errorHandler) {
            code = HomeApi().getConfirmationCode(loggedInUser.Sub_Code ?: "")
            progress.dismiss()

        }
    }


    fun onPressFinish( button: View) {
        if (code == codeConfirmationTF.text.toString()) {
            val intent = Intent(this, UpdateInfoActivity::class.java)
            intent.putExtra("type", type)
            startActivity(intent)
        } else {
            longToast("برجاء التاكد من الكود الصحيح")
        }
    }

}