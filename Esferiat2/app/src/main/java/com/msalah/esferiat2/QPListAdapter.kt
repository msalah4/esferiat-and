package com.msalah.esferiat2

import android.app.Activity
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.msalah.esferiat2.model.MCQuestion
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.q_preview_list_item.view.*
import okhttp3.OkHttpClient
import java.net.URLEncoder

class QPListAdapter(
    val mcqs: ArrayList<MCQuestion>,
    val selectedIndex: Int,
    val context: Activity,
    private val listener: (Int) -> Unit
): RecyclerView.Adapter<QPListAdapter.QPViewHolder>() {

    var clickedIndex: Int = selectedIndex
    override fun getItemCount(): Int {
        return mcqs.size;
    }


    class QPViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        public val image = itemView.qp_imageView
        public val radioButton = itemView.qp_radioButton
        public var index: Int = 0
        //set the onclick listener for the singlt list item

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QPListAdapter.QPViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(
            R.layout.q_preview_list_item,
            parent,
            false
        );
        return QPListAdapter.QPViewHolder(v);
    }

    override fun onBindViewHolder(holder: QPViewHolder, position: Int) {
        holder.radioButton.isChecked = (position == clickedIndex)
        val rawUrl = mcqs[position].image
        val encodedUrl: String = URLEncoder.encode(rawUrl, "utf-8")


        val url: String = Uri.parse(rawUrl)
            .buildUpon()
            .build()
            .toString();
        val client: OkHttpClient =  OkHttpClient();
        val picasso: Picasso =  Picasso.Builder(context)
            .downloader( OkHttp3Downloader(client))
            .build()
        Glide.with(context).load(rawUrl).into(holder.image)
        if (encodedUrl != null) {
            if (!encodedUrl.isEmpty()) {

            }
        }
        holder.index = position
        holder.itemView.setOnClickListener{
            clickedIndex = holder.index
            listener(holder.index)
        }
    }
}