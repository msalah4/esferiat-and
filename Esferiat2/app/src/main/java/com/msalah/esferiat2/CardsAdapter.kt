package com.msalah.esferiat2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_item.view.*

class CardsAdapter (val imagesList: ArrayList<String>): RecyclerView.Adapter<CardsAdapter.CardViewHolder>() {

    override fun getItemCount(): Int {
        return imagesList.size;
    }


    class CardViewHolder (itemView : View):RecyclerView.ViewHolder(itemView) {
        public val image = itemView.card_image_item
        //set the onclick listener for the singlt list item

    }

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        val url = imagesList[position]
        if (!url.isEmpty()) {
            Picasso.get().load(url).into(holder.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.q_preview_list_item, parent, false);
        return CardViewHolder(v);
    }

}