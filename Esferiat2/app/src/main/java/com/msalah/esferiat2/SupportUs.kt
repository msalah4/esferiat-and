package com.msalah.esferiat2

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.msalah.esferiat2.apis.HomeApi
import kotlinx.android.synthetic.main.activity_support_us.*
import kotlinx.coroutines.*
import org.jetbrains.anko.longToast

class SupportUs : AppCompatActivity() {

    val types = arrayListOf<String>("فكرة و اقنراح", "شراكة او تدريب", "دعم مالي")
    lateinit var progress: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_support_us)
        radioGroup.check(R.id.radioButton1)
    }

    fun validateFields () {

        if (phone_support.text.isEmpty() ||
            email_support.text.isEmpty() ||
            idea_support.text.isEmpty() ) {
            longToast("الرجاء ملء جميع الحقول")
            return;
        }
        showSupport()
    }

    fun onPressSubmit( button: View) {
        validateFields()
    }

    fun showSupport() {
        val mainActivityJob = Job()

        progress = ProgressDialog(this)
        progress.setTitle("جاري التحميل")
        progress.setCancelable(false) // disable dismiss by tapping outside of the dialog

        progress.show()
        //2 Handle exceptions if any
        val errorHandler = CoroutineExceptionHandler { _, exception ->
            progress.dismiss()
            AlertDialog.Builder(this).setTitle("Error")
                .setMessage(exception.message)
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }

        //3 the Coroutine runs using the Main (UI) dispatcher
        val coroutineScope = CoroutineScope(mainActivityJob + Dispatchers.Main)
        val context = this
        coroutineScope.launch(errorHandler) {
            //4
            val selectedType = radioGroup.checkedRadioButtonId
            val indexOfType = radioGroup.indexOfChild(radioGroup.findViewById(selectedType))
            val type = types[indexOfType]
            val result = HomeApi().showSupport(type = type,
                note = idea_support.text.toString(),
            phone =  phone_support.text.toString(),
            email = email_support.text.toString()
            )
            progress.dismiss()
            if (result) {
                AlertDialog.Builder(context).setTitle("Success")
                    .setMessage(" شكرا لدعمكم, استقبلنا الطلب بنجاح")
                    .setPositiveButton(android.R.string.ok) { _, _ -> }
                    .setIcon(android.R.drawable.ic_dialog_alert).show()
            } else {
                AlertDialog.Builder(context).setTitle("Error")
                    .setMessage("حدث خطاء ما, الرجاء اعادة المحاولة")
                    .setPositiveButton(android.R.string.ok) { _, _ -> }
                    .setIcon(android.R.drawable.ic_dialog_alert).show()
            }

        }
    }
}