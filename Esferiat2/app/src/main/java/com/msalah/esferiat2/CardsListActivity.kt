package com.msalah.esferiat2

import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.msalah.esferiat2.apis.HomeApi
import kotlinx.android.synthetic.main.activity_cards_list.*
import kotlinx.coroutines.*

class CardsListActivity : AppCompatActivity() {

    private var cardsAdapter = CardsAdapter(arrayListOf())
    lateinit var progress: ProgressDialog
    var listOfImages = ArrayList<String>()
    var downloadLink = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cards_list)


        progress = ProgressDialog(this)
        progress.setTitle("جاري التحميل")
        progress.setCancelable(false) // disable dismiss by tapping outside of the dialog

        progress.show()

        cards_list.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = cardsAdapter
        }

        val mainActivityJob = Job()
        val errorHandler = CoroutineExceptionHandler { _, exception ->
            progress.dismiss()
            AlertDialog.Builder(this).setTitle("Error")
                .setMessage(exception.message)
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }

        val coroutineScope = CoroutineScope(mainActivityJob + Dispatchers.Main)
        val context = this
        coroutineScope.launch(errorHandler) {
            //4
            listOfImages = HomeApi().fetchCards()
            downloadLink = HomeApi().downloadPDF()
            progress.dismiss()

            cardsAdapter = CardsAdapter(listOfImages)
            cards_list.adapter = cardsAdapter
        }


    }

    fun onPressDownload( button: View) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(downloadLink)
        startActivity(intent)
    }

}