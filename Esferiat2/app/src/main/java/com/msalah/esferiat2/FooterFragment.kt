package com.msalah.esferiat2

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.footer_fragment.view.*

class FooterFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        val view = inflater?.inflate(R.layout.footer_fragment,
            container, false)

        view.social_fb.setOnClickListener{v: View -> fbClicked(v)}
        view.social_tw.setOnClickListener{v: View -> twClicked(v)}

        return view
    }

    fun fbClicked( view: View) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse("https://www.facebook.com/Esferiat")
        startActivity(intent)
    }

    fun twClicked( view: View) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse("https://twitter.com/Esferiat")
        startActivity(intent)
    }

}