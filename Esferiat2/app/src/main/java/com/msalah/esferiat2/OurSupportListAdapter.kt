package com.msalah.esferiat2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.msalah.esferiat2.model.OurSupporter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.out_supporter_list_item.view.*

class OurSupportListAdapter(val supportersList: ArrayList<OurSupporter>): RecyclerView.Adapter<OurSupportListAdapter.SupporterViewHolder>() {

    override fun getItemCount(): Int {
        return supportersList.size;
    }


    class SupporterViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        public val image = itemView.our_support_image
        public val title = itemView.our_support_title
        public val desc = itemView.our_support_desc

    }

    override fun onBindViewHolder(holder: SupporterViewHolder, position: Int) {
        val supporter = supportersList[position]
        if (!(supporter.supporterImage!!.isEmpty())) {
            Picasso.get().load(supporter.supporterImage).into(holder.image)
        }
        holder.title.text = supporter.supporterName
        holder.desc.text = supporter.supporterDesc
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SupporterViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.out_supporter_list_item, parent, false);
        return SupporterViewHolder(v);
    }
}