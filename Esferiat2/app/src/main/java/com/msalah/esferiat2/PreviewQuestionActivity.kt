package com.msalah.esferiat2

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.msalah.esferiat2.apis.HomeApi
import com.msalah.esferiat2.model.LoggedInUser
import com.msalah.esferiat2.model.MCQuestion
import com.msalah.esferiat2.model.MonthlyQuestion
import com.msalah.esferiat2.utils.Defaults
import kotlinx.android.synthetic.main.new_list_q_preview.*
import kotlinx.android.synthetic.main.new_q_preview.q_text
import kotlinx.coroutines.*
import org.jetbrains.anko.longToast

class PreviewQuestionActivity : AppCompatActivity() {

    lateinit var progress: ProgressDialog
    lateinit var loggedInUser: LoggedInUser
    lateinit var question: MonthlyQuestion
    lateinit var MCQs: ArrayList<MCQuestion>
    var selectedIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.new_list_q_preview)

        val pqAdapter = QPListAdapter(ArrayList<MCQuestion>(), selectedIndex, this) {
            selectedIndex = it
            preview_q_list.adapter?.notifyDataSetChanged()
        }
        preview_q_list.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = pqAdapter
        }

        loggedInUser = Defaults.getLoggedInUser(this)

        progress = ProgressDialog(this)
        progress.setTitle("جاري التحميل")
        progress.setCancelable(false) // disable dismiss by tapping outside of the dialog

        progress.show()

        val mainActivityJob = Job()
        val errorHandler = CoroutineExceptionHandler { _, exception ->
            progress.dismiss()
            AlertDialog.Builder(this).setTitle("Error")
                .setMessage(exception.message)
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }

        val coroutineScope = CoroutineScope(mainActivityJob + Dispatchers.Main)
        val context = this
        coroutineScope.launch(errorHandler) {
            //4
            question = HomeApi().getMonthQuestion(loggedInUser.Sub_ID ?: "")//SignUpApi().login(emailAddressTF.text.toString(), passwordTF.text.toString())
            MCQs = HomeApi().getMonthQuestionAnswers(question.qID!!)
            progress.dismiss()
            updateOptions()

            question = HomeApi().getMonthQuestion(loggedInUser.Sub_ID ?: "")
//            Picasso.get().load(question.imageURL).into(qImageView)

//            if (question.qAnswer == "-1") {
//                answerTF.isEnabled = false
//                longToast("لا يمكن تعديل اجابتك")
//            } else {
//                answerTF.setText(question.qAnswer ?: "")
//            }
//            val url = (question.imageURL ?: "").replace("http", "https")
            //https://i.imgur.com/tGbaZCY.jpg
//            Picasso.get().load(url).into(qImageView)
        }
    }

    fun updateOptions() {
        val sharedPreference =  getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE)
        val currentLanguage = sharedPreference.getString("lang","ar").toString()

        if (currentLanguage == "ar") {
            q_text.setText(question.qDescreptionAr)
        } else {
            q_text.setText(question.qDescreption)
        }
        val adapter = QPListAdapter(MCQs, selectedIndex, this) {
            selectedIndex = it
            preview_q_list.adapter?.notifyDataSetChanged()
        }
        preview_q_list.adapter = adapter
        adapter.notifyDataSetChanged()


//        if (MCQs.size >= 1) {
//
//
//            val img: ImageView = ImageView(this)
//            Picasso.get().load("https://earth.esa.int/documents/257246/1587672/Sphinx_Pyramid_Giza.jpg").into(
//                img,
//                object : Callback {
//                    override fun onSuccess() {
//                        radio_q1.setCompoundDrawables(img.drawable, null, null, img.drawable)
//                    }
//
//                    override fun onError(e: Exception?) {
//                    }
//
//                }
//            )
//
////            Picasso.get().load(MCQs[0].image).into(radio_q1.imageView)
//        }
//        } else if (MCQs.size >= 2) {
//            Picasso.get().load(MCQs[1].image).into(radio_q2.imageView)
//        } else if (MCQs.size >= 3) {
//            Picasso.get().load(MCQs[2].image).into(radio_q3.imageView)
//        } else if (MCQs.size >= 4) {
//            Picasso.get().load(MCQs[3].image).into(radio_q4.imageView)
//        }
    }

    fun onPressSubmitAnswer( button: View) {

        if (question.qAnswer == "-1") {
           return;
        }

        progress = ProgressDialog(this)
        progress.setTitle("جاري التحميل")
        progress.setCancelable(false) // disable dismiss by tapping outside of the dialog

        progress.show()

        val mainActivityJob = Job()
        val errorHandler = CoroutineExceptionHandler { _, exception ->
            progress.dismiss()
            AlertDialog.Builder(this).setTitle("Error")
                .setMessage(exception.message)
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }

        val coroutineScope = CoroutineScope(mainActivityJob + Dispatchers.Main)
        val context = this
        coroutineScope.launch(errorHandler) {
            //4
//            val selectedType = q_radio_group.checkedRadioButtonId
//            val indexOfType = radioGroup.indexOfChild(radioGroup.findViewById(selectedType))
            val answer = MCQs[selectedIndex].answerId
            val result = HomeApi().submitAnswer(sId = loggedInUser.Sub_ID ?: "",qID = question.qID ?: "", answer = answer ?: "")//SignUpApi().login(emailAddressTF.text.toString(), passwordTF.text.toString())
            progress.dismiss()

            if (result.first == null){
                longToast(result.second ?: "")
                onBackPressed()
            } else {
                longToast(result.first ?: "")
            }

        }
    }
}