package com.msalah.esferiat2.model

class OurSupporter (
    val supporterImage: String?,
    val supporterName: String?,
    val supporterDesc: String?
)