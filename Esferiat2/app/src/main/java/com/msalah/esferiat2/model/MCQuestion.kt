package com.msalah.esferiat2.model

data class MCQuestion (
    val answerId: String?,
    val image: String?,
    val isRight: Boolean?
)
