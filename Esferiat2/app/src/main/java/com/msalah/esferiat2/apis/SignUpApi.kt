package com.msalah.esferiat2.apis

import com.msalah.esferiat2.model.LoggedInUser
import com.msalah.esferiat2.utils.Configurations.Companion.BaseUrl
import okhttp3.OkHttpClient
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class SignUpApi {


    private val service: SignUpService;

    init {

        val okHttpClient = OkHttpClient.Builder()
            .readTimeout(15, TimeUnit.SECONDS)
            .connectTimeout(15, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(BaseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()

        service = retrofit.create(SignUpService::class.java)
    }

    suspend fun login(userName: String, pass: String) : LoggedInUser {
        val result = service.login(userName, pass)
        val root = JSONObject(result)
        val data: JSONArray = root.getJSONArray("data")
        val userJson: JSONObject = data.get(0) as JSONObject
        val user = LoggedInUser(SubName = userJson.getString("SubName"),
            Sub_ID = userJson.getString("Sub_ID"),
            Sub_Code = userJson.getString("Sub_Code"))
        return  user
    }

    suspend fun signUp(fName: String,
                       lName: String,
                       surName: String,
                       email: String,
                       password: String,
                       phone: String,
                       nationality: String,
                       gender: String,
                       howYouKnow:String,
                       birthday:String) : Boolean {
        val result = service.sigup(userName = fName,
        password =  password,
        lastName = lName,
        nickName = surName,
        email = email,
        phone = phone,
        nationality = nationality,
        gender = gender,
        howYouKnow = howYouKnow,
        birthday = birthday)
        val root = JSONObject(result)
        val data: JSONArray = root.getJSONArray("data")
        return  data != null
    }

}