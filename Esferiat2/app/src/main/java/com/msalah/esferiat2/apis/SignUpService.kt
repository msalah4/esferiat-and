package com.msalah.esferiat2.apis

import retrofit2.http.*

interface SignUpService {

    @GET("/ESNew/api/EsferiatServLogin")//?UName=ES03&UPassword=123&type=json")
    suspend fun login(@Query("UName") userName: String, @Query("UPassword") password: String, @Query("type") type: String = "json") : String

    @POST("/ESNew/api/EsferiatServ")
    @FormUrlEncoded
    suspend fun sigup(@Field("Sub_Name") userName: String,
                      @Field("Sub_Password") password: String,
                      @Field("Sub_2Name") lastName: String,
                      @Field("Sub_3Name") nickName: String,
                      @Field("Sub_Email") email: String,
                      @Field("Phone") phone: String,
                      @Field("Nationality") nationality: String,
                      @Field("MaleFemmele") gender: String,
                      @Field("Certifiate") howYouKnow:String,
                      @Field("BirthDate") birthday:String,
                      @Query("type") type: String = "json") : String
}