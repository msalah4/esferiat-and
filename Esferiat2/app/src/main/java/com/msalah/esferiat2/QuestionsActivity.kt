package com.msalah.esferiat2

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.msalah.esferiat2.apis.HomeApi
import com.msalah.esferiat2.model.ExtraUserData
import com.msalah.esferiat2.model.LoggedInUser
import com.msalah.esferiat2.utils.Defaults
import kotlinx.android.synthetic.main.activity_questions.*
import kotlinx.coroutines.*
import java.util.*

class QuestionsActivity : AppCompatActivity() {

    lateinit var progress: ProgressDialog
    lateinit var loggedInUser: LoggedInUser
    lateinit var extraUserData: ExtraUserData
    private var currentLanguage = "en"
    lateinit var locale: Locale

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_questions)
        val name = intent.getStringExtra("name")

        val sharedPreference =  getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE)
        currentLanguage = sharedPreference.getString("lang","ar").toString()
        lang_home_button.setText(currentLanguage.toUpperCase())

        loggedInUser = Defaults.getLoggedInUser(this)
        username.setText(loggedInUser.SubName)
        val mainActivityJob = Job()

        progress = ProgressDialog(this)
        progress.setTitle("جاري التحميل")
        progress.setCancelable(false) // disable dismiss by tapping outside of the dialog

        progress.show()

        val errorHandler = CoroutineExceptionHandler { _, exception ->
            progress.dismiss()
            AlertDialog.Builder(this).setTitle("Error")
                .setMessage(exception.message)
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }

        val coroutineScope = CoroutineScope(mainActivityJob + Dispatchers.Main)
        val context = this
        coroutineScope.launch(errorHandler) {
            //4
            extraUserData = HomeApi().getUserData(loggedInUser.Sub_ID ?: "")//SignUpApi().login(emailAddressTF.text.toString(), passwordTF.text.toString())
            progress.dismiss()

            right_answers.setText(extraUserData.rightAnswers ?: "")
            wrongAnswers.setText(extraUserData.wrongAnswers ?: "")
            my_chances.setText(extraUserData.subscriptions ?: "")
        }
    }

    fun onPressChangePassword( button: View) {
        val intent = Intent(this, UpdateInfoConfirmationCode::class.java)
        intent.putExtra("type", "PASSWORD")
        startActivity(intent)
    }


    fun onPressChangeEmail( button: View) {
        val intent = Intent(this, UpdateInfoConfirmationCode::class.java)
        startActivity(intent)
    }

    fun onPressDeleteAccount( button: View) {

        val mainActivityJob = Job()

        progress = ProgressDialog(this)
        progress.setTitle("جاري التحميل")
        progress.setCancelable(false) // disable dismiss by tapping outside of the dialog

        progress.show()

        val errorHandler = CoroutineExceptionHandler { _, exception ->
            progress.dismiss()
            AlertDialog.Builder(this).setTitle("Error")
                .setMessage(exception.message)
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }

        val coroutineScope = CoroutineScope(mainActivityJob + Dispatchers.Main)
        val context = this
        coroutineScope.launch(errorHandler) {
            onPressLogout(null)
        }

    }

    fun onPressQuestion( button: View) {
        val intent = Intent(this, PreviewQuestionActivity::class.java)
        startActivity(intent)
    }

    fun onPressLogout( button: View?) {
        Defaults.setLoggedInUser(this,null,null, null)
        onBackPressed()
    }

    fun onPressSupporters( button: View) {
        val intent = Intent(this, OurSupportersActivity2::class.java)
        startActivity(intent)
    }

    fun onPressSupportUs( button: View) {
        val intent = Intent(this, SupportUs::class.java)
        startActivity(intent)
    }

    fun onPressCards( button: View) {
        val intent = Intent(this, CardsListActivity::class.java)
        startActivity(intent)
    }


    fun onPressHomeLang(button: View) {

        if (currentLanguage == "ar") {
            setLocale("en")
        } else {
            setLocale("ar")
        }
    }

    private fun setLocale(localeName: String) {
//        if (localeName != currentLanguage) {
        val sharedPreference =  getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE)
        var editor = sharedPreference.edit()
        editor.putString("lang", localeName)
        editor.commit()

        locale = Locale(localeName)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = locale
        res.updateConfiguration(conf, dm)
        val refresh = Intent(
            this,
            MainActivity::class.java
        )
        refresh.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
        refresh.putExtra("name", "")
        startActivity(refresh)
//        } else {
//            Toast.makeText(
//                this@MainActivity, "Language, , already, , selected)!", Toast.LENGTH_SHORT).show();
//        }
    }
}