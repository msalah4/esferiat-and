package com.msalah.esferiat2

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.msalah.esferiat2.apis.HomeApi
import com.msalah.esferiat2.model.LoggedInUser
import com.msalah.esferiat2.utils.Defaults
import kotlinx.android.synthetic.main.activity_update_info.*
import kotlinx.coroutines.*
import org.jetbrains.anko.longToast

enum class UpdateType {
    PASSWORD, EMAIL
}

class UpdateInfoActivity : AppCompatActivity() {

    var updateType = UpdateType.EMAIL
    lateinit var progress: ProgressDialog
    lateinit var loggedInUser: LoggedInUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_info)
        val type = intent.getStringExtra("type")
        updateTF.hint = "الايميل الجديد"
        loggedInUser = Defaults.getLoggedInUser(this)

        if (type == "PASSWORD") {
            updateType = UpdateType.PASSWORD
            updateTF.hint = "الباسورد الجديد"
        }

    }

    fun onPressContinue( button: View) {
        val mainActivityJob = Job()

        progress = ProgressDialog(this)
        progress.setTitle("جاري التحميل")
        progress.setCancelable(false) // disable dismiss by tapping outside of the dialog

        progress.show()
        //2 Handle exceptions if any
        val errorHandler = CoroutineExceptionHandler { _, exception ->
            progress.dismiss()
            AlertDialog.Builder(this).setTitle("Error")
                .setMessage(exception.message)
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }

        //3 the Coroutine runs using the Main (UI) dispatcher
        val coroutineScope = CoroutineScope(mainActivityJob + Dispatchers.Main)
        val context = this
        coroutineScope.launch(errorHandler) {
            //4

            if (updateType == UpdateType.EMAIL) {
                val result = HomeApi().updateUserEmail(updateTF.text.toString(), loggedInUser.Sub_ID ?: "")
                progress.dismiss()
                longToast(result)
                val intent = Intent(context, UpdateInfoConfirmationCode::class.java)
                startActivity(intent)
            } else {
                val result = HomeApi().updateUserPassword(updateTF.text.toString(), loggedInUser.Sub_ID ?: "")
                progress.dismiss()
                longToast(result)

                Defaults.setLoggedInUser(context,null,null, null)

                val intent = Intent(context, MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }

        }
    }
}