package com.msalah.esferiat2.apis

import com.msalah.esferiat2.model.ExtraUserData
import com.msalah.esferiat2.model.MCQuestion
import com.msalah.esferiat2.model.MonthlyQuestion
import com.msalah.esferiat2.model.OurSupporter
import com.msalah.esferiat2.utils.Configurations
import okhttp3.OkHttpClient
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class HomeApi {

    private val service: HomeService;

    init {

        val okHttpClient = OkHttpClient.Builder()
            .readTimeout(15, TimeUnit.SECONDS)
            .connectTimeout(15, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(Configurations.BaseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()

        service = retrofit.create(HomeService::class.java)
    }

    suspend fun getMonthQuestion(sId: String) : MonthlyQuestion {
        val result = service.getMonthQuestion(sId)
        val root = JSONObject(result)
        val data: JSONArray = root.getJSONArray("data")
        val qJson: JSONObject = data.get(0) as JSONObject
        val question = MonthlyQuestion(qID = qJson.getString("Q_ID"),
            qDescreptionAr = qJson.getString("Q_Description"),
            qDescreption = qJson.getString("Q_DescriptionEn"),
            qAnswer = qJson.getString("Q_Answer"))
        return  question
    }

    suspend fun getMonthQuestionAnswers(Id: String) : ArrayList<MCQuestion> {
        val result = service.getMonthQuestionAnswers(Id)
        val root = JSONObject(result)
        val data: JSONArray = root.getJSONArray("data")
        val listOfQas = ArrayList<MCQuestion>()
        for (i in 0 until data.length()) {
            val item = data.getJSONObject(i)
            val qa = MCQuestion( answerId = item.getString("QAnswerID"),
                image = item.getString("Q_Choice"),
                isRight = item.getBoolean("Q_Right"))
            listOfQas.add(qa)
        }
        return  listOfQas
    }

    suspend fun getUserData(sId: String) : ExtraUserData {
        val result = service.getUserData(sId)
        val root = JSONObject(result)
        val data: JSONArray = root.getJSONArray("data")
        val userJson: JSONObject = data.get(0) as JSONObject
        val userData = ExtraUserData(subscriptions = userJson.getString("Subscriptions"),
            rightAnswers = userJson.getString("TrueAnswers"),
            wrongAnswers = userJson.getString("FalseAnswers"))
        return  userData
    }

    suspend fun submitAnswer(sId: String, qID: String, answer: String): Pair<String?, String?> {
        val result = service.submitAnswer(sId, qID, answer)
        if (result.contains("wrong")) {
            return Pair(null, "حدث خطاء ما")
        } else {
            return Pair("تم ارسال اجابتك", null)
        }
    }


    suspend fun updateUserEmail(email: String, sId: String): String {
        val result = service.updateUserEmail(email, sId)
        if (result.contains("wrong")) {
            return "حدث خطاء ما"
        } else {
            return "الرجاء التحقق من بريدك الالكتروني لمعرفة رمز التاكيد"
        }
    }

    suspend fun updateUserPassword(password: String, sId: String): String {
        val result = service.updateUserPassword(password, sId)
        if (result.contains("wrong")) {
            return "حدث خطاء ما"
        } else {
            return "الرجاء التحقق من بريدك الالكتروني لمعرفة رمز التاكيد"
        }
    }

    suspend fun getConfirmationCode(email: String): String {
        val result = service.getConfirmationCode(email)
        val root = JSONObject(result)
        val data: JSONArray = root.getJSONArray("data")
        val codeJson: JSONObject = data.get(0) as JSONObject
        val code = codeJson.getString("AuthCode")
        return  code
    }

    suspend fun deleteAccount(sId: String): Boolean {
        val result = service.deleteAccount(sId)
        val root = JSONObject(result)
        val data: JSONArray = root.getJSONArray("data")
//        val codeJson: JSONObject = data.get(0) as JSONObject
//        val code = codeJson.getString("AuthCode")
        return  data != null
    }


    suspend fun fetchCards(): ArrayList<String> {
        val result = service.fetchCards()
        val root = JSONObject(result)
        val data: JSONArray = root.getJSONArray("data")
        val listOfImagesUrls = ArrayList<String>()
        for (i in 0 until data.length()) {
            val item = data.getJSONObject(i)
            val url = item.getString("C_Path")
            listOfImagesUrls.add(url)
        }
        return  listOfImagesUrls
    }

    suspend fun downloadPDF(): String {
        val result = service.downloadPDF()
        val root = JSONObject(result)
        val data: JSONArray = root.getJSONArray("data")
        val codeJson: JSONObject = data.get(0) as JSONObject
        val code = codeJson.getString("Book_PDFPath")
        return  code
    }

    suspend fun getPartners() : ArrayList<OurSupporter> {
        val result = service.getPartners()
        val root = JSONObject(result)
        val data: JSONArray = root.getJSONArray("data")
        val listOfSupporters = ArrayList<OurSupporter>()
        for (i in 0 until data.length()) {
            val item = data.getJSONObject(i)
            val supporter = OurSupporter( supporterImage = item.getString("P_Path"),
            supporterName = item.getString("P_Name"),
            supporterDesc = item.getString("P_Note"))
            listOfSupporters.add(supporter)
        }
        return  listOfSupporters
    }

    suspend fun showSupport(type: String, note: String, email: String, phone: String): Boolean {
        val result = service.showSupport(type, note, email, phone)
        val root = JSONObject(result)
        val data: JSONArray = root.getJSONArray("data")
        val codeJson: JSONObject = data.get(0) as JSONObject
        val code = codeJson.getString("Code")
        return  code.toLowerCase().contains("done")
    }


}