package com.msalah.esferiat2.model

data class ExtraUserData (

    val subscriptions: String?,
    val rightAnswers: String?,
    val wrongAnswers: String?

)