package com.msalah.esferiat2.apis

import retrofit2.http.GET
import retrofit2.http.Query

interface HomeService {

    @GET("/ESNew/api/EsferiatServGetQuestion")
    suspend fun getMonthQuestion(@Query("Sub_ID") id: String, @Query("type") type: String = "json") : String

    @GET("/ESNew/api/EsferiatServGetQuestChoices")
    suspend fun getMonthQuestionAnswers(@Query("Q_ID") id: String, @Query("type") type: String = "json") : String

    @GET("/ESNew/api/EsferiatServMainData")
    suspend fun getUserData(@Query("Sub_ID") id: String) : String

    @GET("/ESNew/api/EsferiatServAnswer")
    suspend fun submitAnswer(@Query("Sub_ID") id: String , @Query("Q_ID") qID: String, @Query("SubQ_Answer") qAnswer: String, @Query("type") type: String = "json") : String

    @GET("/ESNew/api/EsferiatServUpdateEmail")
    suspend fun updateUserEmail(@Query("NewEmail") email: String, @Query("Sub_ID") sId: String) : String

    @GET("/ESNew/api/EsferiatServUpdatePassword")
    suspend fun updateUserPassword(@Query("NewPassword") password: String, @Query("Sub_ID") sId: String) : String

    @GET("/ESNew/api/EsferiatServLoginAuth")
    suspend fun getConfirmationCode(@Query("UMail") email: String) : String

    @GET("/ESNew/api/EsferiatServDelete")
    suspend fun deleteAccount(@Query("id") sId: String, @Query("type") type: String = "json") : String

    @GET("/ESNew/api/EsferiatServGetCards")
    suspend fun fetchCards(@Query("type") type: String = "json") : String

    @GET("/ESNew/api/EsferiatServGetPDF")
    suspend fun downloadPDF(@Query("type") type: String = "json") : String

    @GET("/ESNew/api/EsferiatServGetParteners")
    suspend fun getPartners(@Query("type") type: String = "json") : String

    @GET("/ESNew/api/EsferiatServSupportRequest")
    suspend fun showSupport(@Query("SupportType") SupportType: String,
                            @Query("SupportNote") SupportNote: String,
                            @Query("SupportEmail") SupportEmail: String,
                            @Query("SupportPhone") SupportPhone: String) : String

}