package com.msalah.esferiat2

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.msalah.esferiat2.apis.SignUpApi
import kotlinx.android.synthetic.main.activity_sigin_up.*
import kotlinx.android.synthetic.main.activity_sigin_up.view.*
import kotlinx.coroutines.*
import org.jetbrains.anko.longToast

class SiginUpActivity : AppCompatActivity() {

    lateinit var progress: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sigin_up)
        gender_radio_group.check(gender_radio_group.male_radioButton.id)
    }


    fun onPressSignup( button: View) {
        validateFields()
    }

    fun validateFields () {

        if (fNameTF.text.isEmpty() ||
            lNameTF.text.isEmpty() ||
            surNameTF.text.isEmpty() ||
            emailTF.text.isEmpty() ||
            cEmailTF.text.isEmpty() ||
            confirmationPasswordTF.text.isEmpty() ||
            passwordTF.text.isEmpty() ||
            phoneTF.text.isEmpty() ||
            birthdayTF.text.isEmpty() ||
            how_to_knowTF.text.isEmpty() ||
            nationalityTF.text.isEmpty()) {
            longToast("الرجاء ملء جميع الحقول")
            return;
        }

        if (passwordTF.text.toString() != confirmationPasswordTF.text.toString()) {
            longToast(" تاكيد الباسورد ليس مثل الباسورد")
            return;
        }

        if (emailTF.text.toString() != cEmailTF.text.toString()) {
            longToast(" تاكيد الايميل ليس مثل الايميل")
            return;
        }

        signUpNewUser()
    }

    fun signUpNewUser() {
        val mainActivityJob = Job()

        progress = ProgressDialog(this)
        progress.setTitle("جاري التحميل")
        progress.setCancelable(false) // disable dismiss by tapping outside of the dialog

        progress.show()
        //2 Handle exceptions if any
        val errorHandler = CoroutineExceptionHandler { _, exception ->
            progress.dismiss()
            AlertDialog.Builder(this).setTitle("Error")
                .setMessage(exception.message)
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }

        //3 the Coroutine runs using the Main (UI) dispatcher
        val coroutineScope = CoroutineScope(mainActivityJob + Dispatchers.Main)
        val context = this
        coroutineScope.launch(errorHandler) {

            val selectedType = gender_radio_group.checkedRadioButtonId
            val indexOfType = gender_radio_group.indexOfChild(gender_radio_group.findViewById(selectedType))
            val genders = arrayListOf<String>("ذكر", "انثي")
            val gender = genders[indexOfType]
            //4
            val result = SignUpApi().signUp(fName = fNameTF.text.toString(),
            lName = lNameTF.text.toString(),
            surName = surNameTF.text.toString(),
            email = emailTF.text.toString(),
            password = passwordTF.text.toString(),
            phone = phoneTF.text.toString(),
            gender = gender,
            nationality = nationalityTF.text.toString(),
            howYouKnow = how_to_knowTF.text.toString(),
            birthday = birthdayTF.text.toString())
            progress.dismiss()
            if (result) {
                val intent = Intent(context, DoneSignUpActivity::class.java)
                startActivity(intent)
            } else {
                AlertDialog.Builder(context).setTitle("Error")
                    .setMessage("حدث خطاء ما, الرجاء اعادة المحاولة")
                    .setPositiveButton(android.R.string.ok) { _, _ -> }
                    .setIcon(android.R.drawable.ic_dialog_alert).show()
            }

        }
    }
}