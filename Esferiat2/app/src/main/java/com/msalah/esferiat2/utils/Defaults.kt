package com.msalah.esferiat2.utils

import android.content.Context
import com.msalah.esferiat2.model.LoggedInUser

class Defaults {

    companion object  {
        val Name = "surName"
        val Id = "sID"
        val Code = "sCode"

        fun getLoggedInUser(context: Context) : LoggedInUser {

            val sharedPreferences = context.getSharedPreferences("shared", 0)

            return LoggedInUser(SubName = sharedPreferences.getString(Name, ""),
                Sub_ID = sharedPreferences.getString(Id, ""),
                Sub_Code = sharedPreferences.getString(Code, ""))
        }

        fun setLoggedInUser(context: Context, name :String?, code :String?, id :String?) {

            val sharedPreferences = context.getSharedPreferences("shared", 0)
            sharedPreferences.edit().putString(Name, name).commit()
            sharedPreferences.edit().putString(Id, id).commit()
            sharedPreferences.edit().putString(Code, code).commit()
            sharedPreferences.edit().apply()

        }
    }

}