package com.msalah.esferiat2

import android.app.ProgressDialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.msalah.esferiat2.apis.HomeApi
import com.msalah.esferiat2.model.OurSupporter
import kotlinx.android.synthetic.main.activity_our_supporters2.*
import kotlinx.coroutines.*

class OurSupportersActivity2 : AppCompatActivity() {

    private var supportersAdapter = OurSupportListAdapter(arrayListOf(OurSupporter(
        supporterImage = "",
        supporterName = "",
        supporterDesc = ""
    )))

    lateinit var progress: ProgressDialog
    var listOfSupporters = ArrayList<OurSupporter>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_our_supporters2)

        our_supporters_list.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = supportersAdapter
        }

        progress = ProgressDialog(this)
        progress.setTitle("جاري التحميل")
        progress.setCancelable(false) // disable dismiss by tapping outside of the dialog

        progress.show()

        val mainActivityJob = Job()
        val errorHandler = CoroutineExceptionHandler { _, exception ->
            progress.dismiss()
            AlertDialog.Builder(this).setTitle("Error")
                .setMessage(exception.message)
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }

        val coroutineScope = CoroutineScope(mainActivityJob + Dispatchers.Main)
        val context = this
        coroutineScope.launch(errorHandler) {
            //4
            listOfSupporters = HomeApi().getPartners()
            progress.dismiss()

            supportersAdapter = OurSupportListAdapter(listOfSupporters)
            our_supporters_list.adapter = supportersAdapter
        }
    }
}