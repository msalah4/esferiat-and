package com.msalah.esferiat2

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.msalah.esferiat2.apis.SignUpApi
import com.msalah.esferiat2.utils.Defaults
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.*
import java.util.*


class MainActivity : AppCompatActivity() {

    lateinit var progress: ProgressDialog
    private var currentLanguage = "en"
    lateinit var locale: Locale

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (!((Defaults.getLoggedInUser(this).SubName ?: "").isEmpty())) {
            val intent = Intent(this, QuestionsActivity::class.java)
            startActivity(intent)
        }

        val sharedPreference =  getSharedPreferences("PREFERENCE", Context.MODE_PRIVATE)
        currentLanguage = sharedPreference.getString("lang","ar").toString()
        lang_main_button.setText(currentLanguage.toUpperCase())
    }

    fun onPressLogin( button: View) {

        val mainActivityJob = Job()

        progress = ProgressDialog(this)
        progress.setTitle("جاري التحميل")
        progress.setMessage("انتظر بينما يتم التحقق من هويتك...")
        progress.setCancelable(false) // disable dismiss by tapping outside of the dialog

        progress.show()
        //2 Handle exceptions if any
        val errorHandler = CoroutineExceptionHandler { _, exception ->
            progress.dismiss()
            AlertDialog.Builder(this).setTitle("Error")
                .setMessage(exception.message)
                .setPositiveButton(android.R.string.ok) { _, _ -> }
                .setIcon(android.R.drawable.ic_dialog_alert).show()
        }

        //3 the Coroutine runs using the Main (UI) dispatcher
        val coroutineScope = CoroutineScope(mainActivityJob + Dispatchers.Main)
        val context = this
        coroutineScope.launch(errorHandler) {
            //4
            val user = SignUpApi().login(emailAddressTF.text.toString(), passwordTF.text.toString())
            progress.dismiss()
            Defaults.setLoggedInUser(this@MainActivity, user.SubName ?: "", user.Sub_Code ?: "", user.Sub_ID ?: "")
            val intent = Intent(context, QuestionsActivity::class.java)
            intent.putExtra("name", user.SubName)
            startActivity(intent)
        }
    }

    fun onPressLang(button: View) {


        if (currentLanguage == "ar") {
            setLocale("en")
        } else {
            setLocale("ar")
        }
    }

    private fun setLocale(localeName: String) {
//        if (localeName != currentLanguage) {
        val sharedPreference =  getSharedPreferences("PREFERENCE",Context.MODE_PRIVATE)
        var editor = sharedPreference.edit()
        editor.putString("lang", localeName)
        editor.commit()

            locale = Locale(localeName)
            val res = resources
            val dm = res.displayMetrics
            val conf = res.configuration
            conf.locale = locale
            res.updateConfiguration(conf, dm)
            val refresh = Intent(
                this,
                MainActivity::class.java
            )
        refresh.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK

        startActivity(refresh)
//        } else {
//            Toast.makeText(
//                this@MainActivity, "Language, , already, , selected)!", Toast.LENGTH_SHORT).show();
//        }
    }

    fun onPressSignUp( button: View) {
        val intent = Intent(this, SiginUpActivity::class.java)
        startActivity(intent)
    }

    fun onPressForgotPassword( button: View) {
        val intent = Intent(this, UpdateInfoConfirmationCode::class.java)
        intent.putExtra("type", "PASSWORD")
        startActivity(intent)
    }
}